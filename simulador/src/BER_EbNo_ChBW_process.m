clc; clear all; close all;

% -- Configurables -- %
EbNo_dB = 0.5:0.5:10;
Ch_BW = 14:1:18;
M = 4;
analizedBER = 1e-3;
% ------------------- %

ChBW_sweeps = length(Ch_BW);
EbNo_sweeps = length(EbNo_dB);

% -- EbNo vs BER for noise added after channel -- %

file_path = "../out/BER_EbNo_ChBW/BER_EbNo_ChBW_noiseAfter.txt";
file = readtable(file_path);

figure
semilogy(file.Var2(1:EbNo_sweeps), berawgn(file.Var2(1:EbNo_sweeps), 'qam', 4).', 'LineWidth', 1.5, 'DisplayName', 'Theorical BER')
hold on
for i = 0:ChBW_sweeps-1
    semilogy(file.Var2(1:EbNo_sweeps), file.Var3(1+EbNo_sweeps*i:EbNo_sweeps+EbNo_sweeps*i), 'LineWidth', 1.5, 'DisplayName', "Channel BW = " + num2str(file.Var5(1+EbNo_sweeps*i)/1e9) + "GHz")
    
    % Calc of approximate EbNo @ analized BER
    [val,idx] = min(abs(file.Var3(1+EbNo_sweeps*i:EbNo_sweeps+EbNo_sweeps*i) - analizedBER));
    a(i+1) = idx; % EbNo indexes for constant BER
end

legend()
ylabel("Bit-Error Rate (BER)", 'Interpreter', 'latex')
xlabel("$E_b/N_0$ [dB]", 'Interpreter', 'latex')
legend('Interpreter', 'latex', 'Location', 'southwest')
xlim([0.5 10])
grid on
set(gcf, 'Position', [100 100 400 400],'Color', 'w')
saveas(gcf, '../out/BER_EbNo_ChBW/BERvsEbN0NoiseAfter.svg', 'svg')

% -- EbNo vs BER for noise added before channel -- %

file_path = "../out/BER_EbNo_ChBW/BER_EbNo_ChBW_noiseBefore.txt";
file = readtable(file_path);

figure
semilogy(file.Var2(1:EbNo_sweeps), berawgn(file.Var2(1:EbNo_sweeps), 'qam', 4).', 'LineWidth', 1.5, 'DisplayName', 'Theorical BER')
hold on
for i = 0:ChBW_sweeps-1
    semilogy(file.Var2(1:EbNo_sweeps), file.Var3(1+EbNo_sweeps*i:EbNo_sweeps+EbNo_sweeps*i), 'LineWidth', 1.5, 'DisplayName', "Channel BW = " + num2str(file.Var5(1+EbNo_sweeps*i)/1e9) + "GHz")
    
    % Calc of approximate EbNo @ analized BER
    [val,idx] = min(abs(file.Var3(1+EbNo_sweeps*i:EbNo_sweeps+EbNo_sweeps*i) - analizedBER));
    b(i+1) = idx; % EbNo indexes for constant BER  
end

legend()
ylabel("Bit-Error Rate (BER)", 'Interpreter', 'latex')
xlabel("$E_b/N_0$ [dB]", 'Interpreter', 'latex')
legend('Interpreter', 'latex', 'Location', 'southwest')
xlim([0.5 10])
grid on
set(gcf, 'Position', [100 100 400 400],'Color', 'w')
saveas(gcf, '../out/BER_EbNo_ChBW/BERvsEbN0NoiseBefore.svg', 'svg')

% -- EbNo Penalty changing channel BW plot -- %

figure
plot(Ch_BW, EbNo_dB(a), '-or', 'LineWidth', 1.5, 'DisplayName', "Noise added after ch, $BER=" + num2str(analizedBER, '%.1e') + "$")
hold on
plot(Ch_BW, EbNo_dB(b), '--ob', 'LineWidth', 1.5, 'DisplayName', "Noise added before ch, $BER=" + num2str(analizedBER, '%.1e') + "$")

legend('Interpreter', 'latex')
ylabel("$E_b/N_0$ [dB]", 'Interpreter', 'latex')
xlabel("Channel BW [GHz]", 'Interpreter', 'latex')
grid on
set(gcf, 'Position', [100 100 400 400],'Color', 'w')
saveas(gcf, '../out/BER_EbNo_ChBW/ChBWPenalty.svg', 'svg')
